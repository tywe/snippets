+++
draft = false
date  = "2024-09-12"
title = "How to find your Bluesky DID PLC ID"

description = "Just go to this page"

summary = "Just go to this page"

tags = ['bluesky', 'quickie']

keywords = ['bluesky', 'quickie']

[author]
    name = "Tyler"
    homepage = "/"

[sitemap]
  changefreq = "monthly"
  priority = 0.5
  filename = "sitemap.xml"
+++

# Intro

I needed a way to find my Bluesky `did:plc` ID code, and search results made this hard. It's actually easy. At least if you're using `bsky.app`.

Just go here: [https://account.bsky.app/user](https://account.bsky.app/user)
