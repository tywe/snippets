+++
draft = false
date  = "2024-07-22"
title = "JMESPath with the AWS CLI - ELBv2 example"

description = "This is an AWS CLI statement that looks up the deregistration delay property for ALBs matching specific criteria"

summary = "This is an AWS CLI statement that looks up the deregistration delay property for ALBs matching specific criteria"

tags = ['jmespath', 'aws', 'cli', 'elb']

keywords = ['jmespath', 'aws', 'cli', 'elb']

[author]
    name = "Tyler"
    homepage = "/"

[twitter]
    site = "@enigmango"

[sitemap]
  changefreq = "monthly"
  priority = 0.5
  filename = "sitemap.xml"
+++

# Intro

I needed a way to list the deregistration delay atttibute for AWS load balancers that met certain config conditions. In this case, HTTP load balancers with instance-type targets (rather than IP targets)

I always find myself looking for working jmespath examples, so here's one. This loops across all LBs, matches filters, then runs another command on the matching ARNs.


# Command

```bash
for arn in $(aws elbv2 describe-target-groups --query "TargetGroups[?TargetType=='instance' && Protocol=='HTTP']" | jq -r ".[].TargetGroupArn"); do echo "------ ${arn} ------"; echo "Delay:";  aws elbv2 describe-target-group-attributes --target-group-arn ${arn} --query "Attributes[?Key=='deregistration_delay.timeout_seconds'].[Value][0][0]" | jq -r ; done
```

Note that you might need to specify `--profile PROFILE_NAME_HERE`.


# Output

Example output:

```
Delay:
90
------ arn:aws:elasticloadbalancing:us-west-2:0xxxxxxxxxx0:targetgroup/snip-name-here/abc000123xyz ------
Delay:
60
------ arn:aws:elasticloadbalancing:us-west-2:0xxxxxxxxxx0:targetgroup/snip-name-here/abc000123xyz ------
Delay:
300
```