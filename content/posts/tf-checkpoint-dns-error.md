+++
draft = false

date = "2021-06-01"

title = "Terraform Checkpoint Error"

description = "An error that looks like a credentials issue at first is actually a DNS issue"

summary = "An error that looks like a credentials issue at first is actually a DNS issue"

tags = ['terraform', 'dns', 'pihole']

keywords = ['terraform', 'dns', 'pihole']

[amp]
    elements = []

[author]
    name = "Tyler"
    homepage = "/"

[image]
    src = "/images/share-banner.png"

[twitter]
    site = "@enigmango"

[sitemap]
  changefreq = "monthly"
  priority = 0.5
  filename = "sitemap.xml"
+++

# Intro

I was trying to run Terraform commands in a directory that I'd previously used with TF with no issues, but today I got the following error:

> Error: No valid credential sources found for AWS Provider.
  Please see https://terraform.io/docs/providers/aws/index.html for more information on
  providing credentials for the AWS Provider

This is weird. The profile config was correct and I'd just used this yesterday.

To help troubleshoot, I set the `TF_LOG` environment variable[^tflog]:

```bash
export TF_LOG=trace
```

and re-run to see this error:

```
[ERR] Checkpoint error: Get https://checkpoint-api.hashicorp.com/v1/check/terraform?arch=amd64&os=darwin&signature=7d7875a0-4705-7036-83f0-6786da79caa5&version=0.12.13: net/http: request canceled while waiting for connection (Client.Timeout exceeded while awaiting headers)
```

...so the actual issue is some kind of timeout.

When I open the HTTPS link in my browser, the request succeeds with a JSON response, so the server isn't down.

Searching around, I found some GitHub issues that indicate this is a DNS issue with the Terraform version or the Go library that handles DNS. I tried uninstalling and reinstalling my version of Terraform (0.12.13) but that didn't help.

# Here's the answer

Finally, I figured it was probably an issue with my DNS server at home, which is a Raspberry Pi running [Pi-hole](https://pi-hole.net/). Sure enough, I tried logging in to the admin web console and got some error messages saying that the FTL service wasn't running.

I SSHed into my Raspberry Pi as an admin user (usually `pi`) and ran the following to restart the service:

```bash
sudo systemctl restart pihole-FTL
```

After that, I was able to successfully run Terraform commands such as `init` and `plan`.

For good measure, I also logged into the admin console and restarted the DNS service.


# Notes

I'm not sure why this happened! I had checked it earlier that day, actually disabling it and re-enabling it for a page that used some tracking that I needed access to. I'll have to watch it more closely, but now I know that if this Checkpoint error comes up in Terraform it's probably my Raspberry Pi.

[^tflog]: The [documentation instructions](https://www.terraform.io/docs/internals/debugging.html) vary a little bit, but it seems like the [only really valid options](https://www.terraform.io/docs/cli/config/environment-variables.html) are `trace` and `off` for newer versions of TF.
